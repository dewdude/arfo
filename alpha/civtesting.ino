/* ArFO Development Version
 *  
 * This software doesn't control the radio yet. It is just to test working with the
 * iCom CI-V data stream coming from the radio.
 * 
 * de NQ4T - 08-FEB-2020
 */

const byte numBytes = 32;
byte receivedBytes[numBytes];
byte numReceived = 0;
long vfoa;

boolean newData = false;

void setup() {
    Serial.begin(9600);
    Serial2.begin(9600);
    Serial.println("CI-V Monitor Ready");
    Serial.println();
    
}

void loop() {
    recvBytesWithStartEndMarkers();
    showNewData();
    newData = false;
}

void recvBytesWithStartEndMarkers() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    byte startMarker = 0xFE;
    byte endMarker = 0xFD;
    byte rb;
   

    while (Serial2.available() > 0 && newData == false) {
        rb = Serial2.read();

        if (recvInProgress == true) {
            if (rb != endMarker) {
                receivedBytes[ndx] = rb;
                ndx++;
                if (ndx >= numBytes) {
                    ndx = numBytes - 1;
                }
            }
            else {
                receivedBytes[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                numReceived = ndx;  // save the number for use when printing
                ndx = 0;
                newData = true;
            }
        }

        else if (rb == startMarker) {
            recvInProgress = true;
        }
    }
}
void processciv() {
  int i = 0;
  long bcd[4];
    for (int x = numReceived; x > 2; x--) {
      bcd[i] = (((receivedBytes[x] >> 4) *10) + (receivedBytes[x]&0xF));
      i++;
    }
    vfoa = ((bcd[0]*1000000)+(bcd[1]*10000)+(bcd[2]*100)+(bcd[3]));
    }
void showNewData() {
    if (newData == true) {
       numReceived--;
       processciv();
       Serial.println();
       Serial.print("VFO A: ");
       Serial.print(vfoa, DEC);
       Serial.print("kHz");     
    }
    newData = false;
    }
